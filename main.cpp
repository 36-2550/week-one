#include <iostream>

using namespace std;
int main(){
  //cout is to write out
  //endl is the equivalent of \n but also calls flush
  //both cout and endl are members of the std namespace
  cout << "Hello World" << endl;


  //the standard library has a string type 
  string s = "1000";
  //also a proper boolean type
  bool b = false;

  //functions work the same as in c, order matters!
}

