//Include statement to include standard IO functionality
//can also be written #include <"stdio.h">
//.h indicates it is a header file
#include "stdio.h"

//stub function(s)
void foo(int i);

void bar(int *i);

//integer constant
//pre processor directive
#define MY_CONST 10

int main(){
  //default data types
  int i = 10;
  float f = 10.0;
  double d = 11.0000000;
  char * cs = "hello world";

  //writing out strings
  //with new line escape string
  printf("%s\n", cs);

  //writing out numbers
  printf("%d", i);

  //will result in i = 10
  foo(i);
  //will result in i = 20
  bar(&i);

  //control loops

  while(i < 10){
    //do something
  }

  do{
    //do something
  } while(i < 10);

  for(int j = 0; j < i; ++j){
    //do something 10 times
  }
}

//Function definition (pass by value)
//initial value of i will not be changed
//only the local copy's
void foo(int i){
  i = 10;
}

//function definition (pass by reference)
//initial value will be changed
void bar(int * i){
  //* dereferenced to get value
  *i = 10;
}


